const fs = require('fs');
const crypto = require('crypto');

function checksum (argv) {
  var message = '';
  try {
    fs.readFile(argv.dir, function (err, data) {
      let checksum = getMd5Checksum(data);
      message = `${argv.dir} -> md5:${checksum}`;
      console.log(message);
    });
  } catch (err) {
    console.error('Failed to get checksum because', err);
  }
  return message;
}

function getMd5Checksum (str, encoding) {
  return crypto
    .createHash('md5')
    .update(str, 'utf8')
    .digest(encoding || 'hex');
}

// Expose command to yargs
exports.command = 'checksum [dir]';
exports.aliases = ['cs'];
exports.desc = 'Generate hash of target text-file.';
exports.builder = {
  dir: {
    default: '.'
  }
};

exports.handler = checksum;

// Exposing for unit tests purposes only. '_' denotes private
exports._checksum = checksum;
exports._getMd5Checksum = getMd5Checksum;
