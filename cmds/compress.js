const zlib = require('zlib');
const gzip = zlib.createGzip(); //
const fs = require('fs');

function compress (argv) {
  try {
    let fileName = argv.dir;
    let newFileName = fileName.replace('.gz', '') + '.gz'; // Guarantee that the extension '.gz' occur only once
    const input = fs.createReadStream(fileName);
    const output = fs.createWriteStream(newFileName);
    input.pipe(gzip).pipe(output);
    console.log(`Compressed '${fileName}' into '${newFileName}'`);
  } catch (err) {
    console.error(`Failed to decompress ${argv.dir}.`, err);
  }

}

// Expose command to yargs
exports.command = 'compress [dir]';
exports.aliases = ['c'];
exports.desc = 'Compress target text-file.';
exports.builder = {
  dir: {
    default: '.'
  }
};
exports.handler = compress;

// Exposing for unit tests purposes only. '_' denotes private
exports._compress = compress;
