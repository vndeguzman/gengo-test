const fs = require('fs');

function size (argv) {
  try {
    let fileName = argv.dir;
    let stats = fs.statSync(fileName);
    let fileSizeInBytes = stats["size"];
    console.log(`${fileName} is ${fileSizeInBytes} bytes`);
  }
  catch (err) {
    console.log('Failed to get size because', err);
  }

}

// Expose command to yargs
exports.command = 'size [dir]';
exports.aliases = ['s'];
exports.desc = 'Get size of target text-file.';
exports.builder = {
  dir: {
    default: '.'
  }
};
exports.handler = size;

// Exposing for unit tests purposes only. '_' denotes private
exports._size = size;
