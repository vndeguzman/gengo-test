const zlib = require('zlib');
const gunzip = zlib.createGunzip();
const fs = require('fs');

function decompress (argv) {
  try {
    let fileName = argv.dir;
    let newFileName = fileName.replace('.gz', '');
    const input = fs.createReadStream(fileName);
    const output = fs.createWriteStream(newFileName);
    if (/.gz$/i.test(fileName)) {
      input.pipe(gunzip).pipe(output);
      console.log(`Decompressed ${fileName} into '${newFileName}'`);
    }
    else {
      console.error(`Failed to decompress '${fileName}' because file must end with '.gz'`);
    }
  } catch (err) {
    console.error(`Failed to decompress ${fileName} because`, err);
  }
}

// Expose command to yargs
exports.command = 'decompress [dir]';
exports.aliases = ['dc'];
exports.desc = 'Decompress target text-file.';
exports.builder = {
  dir: {
    default: '.'
  }
};
exports.handler = decompress;

// Exposing for unit tests purposes only. '_' denotes private
exports._decompress = decompress;
