const expect = require('chai').expect;
const describe = require('mocha').describe;
const it = require('mocha').it;

const checksum = require('./cmds/checksum')._checksum;
const compress = require('./cmds/compress')._compress;
const decompress = require('./cmds/decompress')._decompress;
const size = require('./cmds/size')._size;

describe('checksum()', function () {
  it('should return file name and md5 hash', function () {
    // const expected = 'sample/test.txt -> md5:5f2453a94c841c650e3348ee09399969';
    // const actual = checksum({dir: 'sample/test.txt'});
    // expect(actual).equal(expected);
  });
});

describe('compress()', function() {
  it('should generate .gz file', function() {

  })
})

describe('decompress()', function() {
  it('should generate plain text file', function() {

  })
})

describe('size()', function() {
  it('should return file size in bytes', function() {

  })
})