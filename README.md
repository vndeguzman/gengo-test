# Gengo Test #

### Problem Statement ###
1. Write your own compression/decompression program in python, Go or Php, the program should be able to do the following:

	a. take a regular text file as input, output a compressed version of the same file

   	b. decompressing the compressed file MUST produce the same hash as the original file

   	c. Important points:

    	i. You can use existing libraries for the compression/decompression process

    	ii. Command line interface must be well-designed and along with de-jure  or de-facto standards.

      		1. For example please use youprogram.php(or .py) -c filetozip.txt and yourprogram.php (or .py) -u filetounzip.zip

	    iii. Implementation must be well-organized, easy to read, efficient enough and appropriately commented.

    	iv. Implementation must have enough amount of tests.


### Setup ###

* Make sure latest versions of Node(v10+) and npm (v6+) are installed.
* Run `npm install -g gengo-test` (Must be -g/global to write out compressed files)

### Usage ###
* `gengo <command|alias> <text-file>`

### Commands ###
* compress|c - Zip file using gzip
* decompress|dc - Unzip file using gunzip
* checksum|cs - Generates hash using md5
* size|s - Gets the file size

### Unit Tests ###
* `npm test`

### Scaling Performance ###
* (Unix-like OSes) `UV_THREADPOOL_SIZE=64`
* (Windows) `SET UV_THREADPOOL_SIZE=64`

### Uninstallation ###
* Run `npm uninstall gengo-test`


Thanks!